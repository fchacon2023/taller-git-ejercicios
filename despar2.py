def des_par(n: int) -> bool:
    """Requiere: nada
       Devuelve: True si n es par, False si es impar
    """
    b: bool = (n % 2 == 0)
    if b == True:
        vr = "El numero es par"
    else:
        vr = "El numero es impar"
    return b, vr

print(des_par(7)) 
print(des_par(8)) 
